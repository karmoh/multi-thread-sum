package com.multi.sum;

import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;

class MultiThreadSumServletIT {

    private final String HOST = "http://localhost:8999/";

    @Test
    void multiThreadSumIntegrationTest() throws IOException, InterruptedException, ExecutionException {
        var httpClient = HttpClient.newBuilder().connectTimeout(Duration.ofMinutes(10)).build();

        var responseFuture21 = createAsyncRequest(httpClient, "21");
        var responseFuture5 = createAsyncRequest(httpClient, "5");
        var responseFuture5c = createAsyncRequest(httpClient, "5c");
        waitASecond();
        var responseEnd1 = httpClient.send(createRequest("end"),HttpResponse.BodyHandlers.ofString());

        createAsyncRequest(httpClient, "41");
        createAsyncRequest(httpClient, "16");
        waitASecond();
        var responseEnd2 = httpClient.send(createRequest("end"),HttpResponse.BodyHandlers.ofString());

        assertEquals("26", responseEnd1.body());
        assertEquals(200, responseEnd1.statusCode());
        assertEquals("26", responseFuture21.get().body());
        assertEquals("26", responseFuture5.get().body());
        assertEquals(400, responseFuture5c.get().statusCode());

        assertEquals("57", responseEnd2.body());
        assertEquals(200, responseEnd2.statusCode());
    }

    private void waitASecond(){
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
    private CompletableFuture<HttpResponse<String>> createAsyncRequest(HttpClient httpClient, String value) {
        return httpClient.sendAsync(createRequest(value), HttpResponse.BodyHandlers.ofString());
    }

    private HttpRequest createRequest(String value) {
        return HttpRequest.newBuilder()
                .uri(URI.create(HOST))
                .timeout(Duration.ofSeconds(2))
                .POST(HttpRequest.BodyPublishers.ofString(value))
                .build();
    }


}