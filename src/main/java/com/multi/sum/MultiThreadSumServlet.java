package com.multi.sum;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.WriteListener;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

@WebServlet(asyncSupported = true, name = "MultiThreadSumServlet", urlPatterns = {"/"})
public class MultiThreadSumServlet extends HttpServlet {

    private final static Logger LOG = LoggerFactory.getLogger(MultiThreadSumServlet.class);
    private final Map<UUID, Long> initialValueMap = new ConcurrentHashMap<>();
    private final Map<UUID, Long> endSumMap = new ConcurrentHashMap<>();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        var async = request.startAsync();
        var out = response.getOutputStream();
        out.setWriteListener(new WriteListener() {
            @Override
            public void onWritePossible() throws IOException {
                var id = UUID.randomUUID();
                var body = extractRequestBody(request);
                LOG.info("request {} with body {}", id, body);
                if ("end".equalsIgnoreCase(body)) {
                    sumRunningThreads(id);
                } else {
                    addValueAndWaitForResponse(id, body);
                }
                if (out.isReady()) {
                    response.setStatus(200);
                    async.complete();
                    var endValue = endSumMap.get(id);
                    LOG.info("Sum for {} is {}", id, endValue);
                    out.write(String.valueOf(endValue).getBytes(StandardCharsets.UTF_8));
                    endSumMap.remove(id);
                } else {
                    throw new RuntimeException("Response timeout");
                }

            }

            @Override
            public void onError(Throwable t) {
                getServletContext().log("Async Error", t);
                response.setStatus(400);
                async.complete();
            }
        });
    }

    private void sumRunningThreads(UUID id) {
        Set<UUID> endIdSet = new HashSet<>();
        var sum = new AtomicLong(0);
        initialValueMap.forEach((uuid, value) -> {
            endIdSet.add(uuid);
            sum.addAndGet(value);
        });
        endIdSet.forEach(uuid -> {
            initialValueMap.remove(uuid);
            endSumMap.put(uuid, sum.get());
        });
        endSumMap.put(id, sum.get());
    }

    private void addValueAndWaitForResponse(UUID id, String body) {
        initialValueMap.put(id, Long.parseLong(body));
        while (!endSumMap.containsKey(id)) {
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }

    private String extractRequestBody(HttpServletRequest request) throws IOException {
        var body = request.getReader().lines().findFirst();
        if (body.isEmpty()) {
            throw new RuntimeException("Body required");
        }
        return body.get();
    }
}