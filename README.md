# Multi thread sum application

### Prerequisites

You will need Java 14

## Running application

```
mvn jetty:run
```

application will run on localhost:8999

#### Request examples:
```
curl -d 134 http://localhost:8999/

curl -d end http://localhost:8999/
```
## Running the tests

```
mvn verify
```

## Create runnable jar for deployment

```
mvn package
```

jar location: /target/MultiThreadSum-1.0-SNAPSHOT.jar

## Built With

* Java14
* Jetty-maven-plugin
* Maven-failsafe-plugin
* Maven-shade-plugin
* JUnit
